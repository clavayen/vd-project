angular.module('vd.controllers', ['ui.bootstrap'])

.controller('alertMessagesController',[ '$scope','$rootScope','AlertService', function ($scope, $rootScope, AlertService ) {
	this.alerts = $rootScope.alerts;
}])

.controller('AppCtrl', [ '$scope' ,'NAVIGATION_ACTION','$rootScope', function($scope, navigationAction,$rootScope){
	
	$scope.action = 0;
		
	$scope.setAction = function(pAction){
	   $scope.action = pAction;
	};
	$scope.getAction = function(){
	   return $scope.action;
	};
	
	$scope.getActionName = function(){
		switch ($scope.action) {
			case navigationAction.ACTION_LIST:
				return "Listado";
			case navigationAction.ACTION_EDIT:
				return "Edicion";
			case navigationAction.ACTION_DELETE:
				return "Eliminar";
			case navigationAction.ACTION_NEW:
				return "Nuevo";
			case navigationAction.ACTION_VIEW:
				return "Ver";
		}
		return "";
	};

	$scope.isDisabled = function () {
		switch ($scope.action) {
			case navigationAction.ACTION_VIEW:
			case navigationAction.ACTION_DELETE:
				return true;
		}
		return false;
	};
        
}])
