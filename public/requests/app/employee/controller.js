angular.module('vd.employee', ['vd.controllers','ui.bootstrap','ui.bootstrap.datetimepicker','ui.router'])

    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
        .state('employee', {
            url: '/employee',
            templateUrl: 'content.tpl.html',
            resolve: {
            }
        })
        .state('employee.list', {
            url: '/position/:id/list',
            templateUrl: 'app/employee/employee-list.html',
            controller: 'EmployeeListCtrl',
            method: 'list',
            resolve: {
                areas: function (AreaService,$stateParams) {
                    return AreaService.getAll();
                },
                position: function (PositionService,$stateParams) {
                    var id = $stateParams.id;
                    return PositionService.get(id);
                },
                employees: function (EmployeeService,$stateParams) {
                    var id = $stateParams.id;
                    return EmployeeService.getByPosition(id);
                },
                positionId: function (EmployeeService,$stateParams) {
                    return ($stateParams.id==='')?null:$stateParams.id;
                }
            }
        })
        .state('employee.new', {
            url: '/nuevo',
            templateUrl: 'app/employee/employee.html',
            controller: 'EmployeeCtrl',
            method: 'create',
        })
}])

.controller('EmployeeListCtrl', ['$scope', '$controller', 'NAVIGATION_ACTION','PositionService','EmployeeService','$state','areas','position','employees','positionId',
    function ($scope, $controller, NAVIGATION_ACTION, PositionService, EmployeeService, $state,areas,position,employees,positionId) {

        $controller('AppCtrl', {$scope: $scope,navigationAction:NAVIGATION_ACTION });
        $scope.setAction(NAVIGATION_ACTION.ACTION_NEW);

        $scope.areas = areas.data;
        $scope.positions = position.data;
        $scope.employees = employees.data;
        $scope.positionId = positionId

        $scope.filterCriteria = {
            areaId: null,
            positionId: parseInt($scope.positionId)
        }

        $scope.changeArea = function(areaId){
            if(areaId !== null && areaId !== ''){
                $scope.getPositionsByArea(areaId);
            }
        }

        $scope.getPositionsByArea = function(areaId){
            return PositionService.getByArea(areaId)
                .then(function(response) {
                    $scope.positions = response.data
                }, function(response) {
                    $timeout(function(){swal("Error!",response.data.error,"error")}, 300)
                });
        }

        $scope.searchEmployees = function(idPosition){
            if(idPosition !== null && idPosition !== ''){
                $scope.employees = [];
                $scope.getEmployeesByPositions(idPosition);
            }
        }

        $scope.getEmployeesByPositions = function(positionId){
            return EmployeeService.getByPosition(positionId)
                .then(function(response) {
                    $scope.employees = response.data
                }, function(response) {
                    $timeout(function(){swal("Error!",response.data.error,"error")}, 300)
                });
        }

        $scope.cancel = function(){
            $state.go('area.list');
        };

        $scope.init = function(){
        }

        $scope.init();

}])