angular.module('vd.position', ['vd.controllers','ui.bootstrap','ui.bootstrap.datetimepicker','ui.router'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('position', {
            url: '/puestos',
            templateUrl: 'content.tpl.html',
            resolve: {
            }
        })
        .state('position.list', {
            url: '/list',
            templateUrl: 'app/position/position-list.html',
            controller: 'PositionListCtrl',
            method: 'list',
        })
        .state('position.new', {
            url: '/new/:parentId',
            templateUrl: 'app/position/position.html',
            controller: 'PositionCtrl',
            method: 'create',
            resolve: {
                areas: function (AreaService) {
                    return AreaService.getAll();
                },
                parentId: function ($stateParams) {
                    return ($stateParams.parentId==='')?null:$stateParams.parentId;
                }
            }
        })
}])

.controller('PositionListCtrl', ['$scope', '$controller', 'NAVIGATION_ACTION','PositionService','$state',
    function ($scope, $controller, NAVIGATION_ACTION, PositionService, $state) {

        $controller('AppCtrl', {$scope: $scope,navigationAction:NAVIGATION_ACTION });
        $scope.setAction(NAVIGATION_ACTION.ACTION_NEW);

        $scope.position = {
            name: '',
            type: '',
            parent: ''
        }

        $scope.cancel = function(){
            $state.go('area.list');
        };

        $scope.init = function(){
        }

        $scope.init();

}])

.controller('PositionCtrl', ['$scope', '$controller', 'NAVIGATION_ACTION','PositionService','$state','$timeout','areas','parentId',
    function ($scope, $controller, NAVIGATION_ACTION, PositionService, $state,$timeout,areas,parentId) {

        $controller('AppCtrl', {$scope: $scope,navigationAction:NAVIGATION_ACTION });
        $scope.setAction(NAVIGATION_ACTION.ACTION_NEW);

        $scope.parents = areas.data;
        $scope.areaId = (parentId!==null)?parseInt(parentId):parentId;


        $scope.position = {
            name: '',
            type: null,
            areaId: $scope.areaId
        }

        function getData(){
            return data = {
                name: $scope.position.name,
                area_id: $scope.position.areaId
            }
        }

        $scope.save = function(){
            swal({
                    title: "Atencion",
                    text: "¿Desea guardar el puesto?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(resp){
                    if(resp){
                        var data = getData();
                        return PositionService.save(data)
                            .then(function(response) {
                                if(response.data){
                                    $timeout(function(){swal("Exito!","El puesto se registro correctamente","success")}, 300)
                                    $state.go("area.list");
                                }
                            }, function(response) {
                                $timeout(function(){swal("Error!",response.data.error,"error")}, 300)
                            });

                    }
                });
        }

        $scope.cancel = function(){
            $state.go('area.list');
        };

        $scope.init = function(){
        }

        $scope.init();

}])