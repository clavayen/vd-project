angular.module('vd.area', ['vd.controllers','ui.bootstrap','ui.bootstrap.datetimepicker','ui.router'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('area', {
            url: '/areas',
            templateUrl: 'content.tpl.html',
            resolve: {
            }
        })
        .state('area.list', {
            url: '/list',
            templateUrl: 'app/area/area-list.html',
            controller: 'AreaListCtrl',
            method: 'list',
        })
        .state('area.new', {
            url: '/nuevo/:parentId',
            templateUrl: 'app/area/area.html',
            controller: 'AreaCtrl',
            method: 'create',
            resolve: {
                areas: function (AreaService) {
                    return AreaService.getAll();
                },
                parentId: function ($stateParams) {
                    return ($stateParams.parentId==='')?null:$stateParams.parentId;
                }
            }
        })
}])

.controller('AreaListCtrl', ['$scope', '$controller', 'NAVIGATION_ACTION','AreaService','PositionService','$state',
    function ($scope, $controller, NAVIGATION_ACTION, AreaService, PositionService, $state) {

        $controller('AppCtrl', {$scope: $scope,navigationAction:NAVIGATION_ACTION });
        $scope.setAction(NAVIGATION_ACTION.ACTION_LIST);

        $scope.label = {
            new: '',
            edit: '',
            remove: '',
            employee: ''
        }

        $scope.new = function(){
            $state.go('places.new');
        };

        function setPropertyAreas(areaParentId,areas){
            var data = new Array();
            angular.forEach(areas,function(item){
                obj = {
                    "id": areaParentId+'/'+item.id,
                    "real_id": item.id,
                    "text": item.name,
                    "children": true,
                    "type": "area",
                    "parent_id": areaParentId
                }
                data.push(obj);
            })
            return data
        }

        function setPropertyPositions(areaParentId,positions){
            var data = new Array();
            angular.forEach(positions,function(item){
                obj = {
                    "id": areaParentId+'/pos_'+item.id,
                    "real_id": item.id,
                    "text": item.name,
                    "children": false,
                    "type": "position",
                    "parent_id": areaParentId
                }
                data.push(obj);
            })
            return data
        }

        function getAreasRoot(cb){
            return AreaService.getRoot()
            .then(function(response) {
                if(response.data){
                    var nodes = response.data
                    var arr = [];
                    nodes.forEach(function(element) {
                        arr.push({
                            "id": element.id,
                            "text": element.name,
                            "children": true,
                            "real_id": element.id,
                            "type": "area",
                            "parent_id": null
                        });
                    });
                    cb(arr);
                }
            }, function(response) {

            });
        }

        function getAreasChild(cb,areaId){
            return AreaService.getChilds(areaId)
                .then(function(response) {
                    if(response.data){
                        getPositionsByArea(cb,response.data,areaId)
                    }
                }, function(response) {

                });
        }

        function getPositionsByArea(cb,areas,areaId){
            return PositionService.getByArea(areaId)
                .then(function(response) {
                    if(response.data){
                        var positions = response.data;
                        concatAreasWhitPositions(areas,positions,cb,areaId)
                    }
                }, function(response) {

                });
        }

        function concatAreasWhitPositions(areas,positions,cb,areaParentId){
            areas = setPropertyAreas(areaParentId,areas);
            positions = setPropertyPositions(areaParentId,positions);
            node = areas.concat(positions)
            var arr = [];
            node.forEach(function(element) {
                arr.push({
                    "id": element.id,
                    "text": element.text,
                    "children": element.children,
                    "real_id": element.real_id,
                    "type": element.type,
                    "parent_id": element.parent_id
                });
            });
            cb(arr);
            return areas;
        }

        function initTree(){
            $('#tree').jstree({
                'core' : {
                    'data' : function (node, cb) {
                        if(node.id === "#") {
                            getAreasRoot(cb);
                        }else {
                            getAreasChild(cb,node.original.real_id);
                        }
                    },
                    "multiple": false,
                    "check_callback": false
                },
                "plugins" : ["contextmenu","types"],
                "contextmenu":{
                    "items": function(node) {
                        var tree = $("#tree").jstree(true);
                        return getContextByTypeNode(node,tree);
                    }
                },
                "types" : {
                    "area" : {
                        "icon" : "glyphicon glyphicon-home"
                    },
                    "position" : {
                        "icon" : "glyphicon glyphicon-user"
                    }
                },
            }).on('select_node.jstree', function(event, data) {
                event.preventDefault();
                if (data && data.selected && data.selected.length) {
                };
            })
        }

        function getContextByTypeNode(node,tree){
            switch(node.type) {
                case 'area':
                    $scope.label.new = "Nueva Area";
                    $scope.label.edit = "Editar Area";
                    $scope.label.remove = "Eliminar Area";
                    $scope.label.employee = "Ver Empleados";
                    $scope.label.newPosition = "Nuevo Puesto";
                    break;
                case 'position':
                    $scope.label.new = "Nuevo Puesto";
                    $scope.label.edit = "Editar Puesto";
                    $scope.label.remove = "Eliminar Puesto";
                    $scope.label.employee = "Ver Empleados";
                    break;
            }

            var create = {
                "Create": {
                    "separator_before": false,
                    "separator_after": false,
                    "label": $scope.label.new,
                    "action": function (obj) {
                        if(node.type === 'area'){
                            $state.go('area.new',{parentId: node.original.real_id})
                        }else{
                            $state.go('position.new',{parentId: node.original.parent_id})
                        }
                    }
                }
            }
            var createPosition = {
                "CreatePosition": {
                    "separator_before": false,
                    "separator_after": true,
                    "label": $scope.label.newPosition,
                    "action": function (obj) {
                        $state.go('position.new',{parentId: node.original.real_id})
                    }
                }
            }
            var rename = {
                "Rename": {
                    "separator_before": false,
                    "separator_after": false,
                    "label": $scope.label.edit,
                    "action": function (obj) {
                        if(node.type === 'area'){
                            // $state.go('area.edit',{parentId: node.original.parent_id})
                        }else{
                            // $state.go('position.edit',{parentId: node.original.parent_id})
                        }
                    }
                },
            }
            var remove = {
                "Remove": {
                    "separator_before": true,
                    "separator_after": false,
                    "label": $scope.label.remove,
                    "action": function (obj) {
                        if(node.type === 'area'){

                        }else{

                        }
                    }
                },
            }
            var employees = {
                "Employee": {
                    "separator_before": true,
                    "separator_after": false,
                    "label": $scope.label.employee,
                    "action": function (obj) {
                        if(node.type === 'area'){
                            console.log("imposible")
                        }else{
                            $state.go('employee.list',{id: node.original.real_id})
                        }
                    }
                }
            }

            switch(node.type) {
                case 'area':
                    var context = Object.assign({}, createPosition, create, rename, remove);
                    break;
                case 'position':
                    var context = Object.assign({}, rename, remove, employees);
                    break;
            }

            return context;
        }

        $scope.init = function(){
            initTree();
        }

        $scope.init();

}])

.controller('AreaCtrl', ['$scope', '$controller', 'NAVIGATION_ACTION','AreaService','$state','$timeout','areas','parentId',
    function ($scope, $controller, NAVIGATION_ACTION, AreaService, $state,$timeout, areas,parentId) {

        $controller('AppCtrl', {$scope: $scope,navigationAction:NAVIGATION_ACTION });
        $scope.setAction(NAVIGATION_ACTION.ACTION_NEW);

        $scope.parents = areas.data;
        $scope.parentId = (parentId!==null)?parseInt(parentId):parentId;

        $scope.types = [
            {
                description: 'AREA',
                id: 1
            },
            {
                description: 'PUESTO',
                id: 2
            }
        ]

        $scope.area = {
            name: '',
            type: 1,
            parentId: $scope.parentId
        }

        function getData(){
            return data = {
                name: $scope.area.name,
                parent_id: $scope.area.parentId
            }
        }

        $scope.save = function(){
            swal({
                    title: "Atencion",
                    text: "¿Desea guardar el area?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(resp){
                    if(resp){
                        var data = getData();
                        return AreaService.save(data)
                        .then(function(response) {
                            if(response.data){
                                $timeout(function(){swal("Exito!","El area se registro correctamente","success")}, 300);
                                $state.go("area.list");
                            }
                        }, function(response) {
                            $timeout(function(){swal("Error!",response.data.error,"error")}, 300)
                        });

                    }
                });
        }

        $scope.cancel = function(){
            $state.go('area.list');
        };

        $scope.init = function(){
        }

        $scope.init();

}])