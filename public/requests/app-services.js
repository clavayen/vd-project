
angular.module('vd.services', ['restangular', 'angular-loading-bar'])

.config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.setBaseUrl(URL_BASE_SERVER);
}])

.service('AlertService', function AlertService($rootScope, $timeout) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    // create an array of alerts available globally
    $rootScope.alerts = [];

    var alertService = {
        /**
         * Add a alert in application
         * #### Example of use
         * <pre><code>AlertService.add('success', 'This is a alert with 5 seconds of lifetime!', 5000);</pre></code>
         * @param {String} type    alert type
         * @param {String} msg     alert message
         * @param {Integer} timeout alert lifetime
         * @method add
         */
        add: function (type, msg, timeout) {
            $rootScope.alerts.push({
                type: type,
                msg: msg
            });
            if (timeout) {
                var index = $rootScope.alerts.length - 1;
                $timeout(function () {
                    alertService.closeAlertMessage(index);
                }, timeout);
            }
        },
        /**
         * Close a specific alert in application
         * #### Example of use
         * <pre><code>AlertService.closeAlertMessage(0);</pre></code>
         * @param  {Integer} index alert index in $rootScope.alerts array
         * @method closeAlertMessage
         * @return {Boolean}
         */
        closeAlertMessage: function (index) {
            return $rootScope.alerts.splice(index, 1);
        },
        /**
         * Clear all alert messages in application
         * #### Example of use
         * <pre><code>AlertService.clear();</pre></code>
         * @method clear
         */
        clear: function () {
            $rootScope.alerts = [];
        }
    };
    $rootScope.closeAlertMessage = alertService.closeAlertMessage;
    return alertService;
})

.service('AreaService', ['Restangular', function (Restangular) {
    return {
        search: function (currentPage, sortField, sortDirection, criteria) {
            var params = {
                            currentPage: currentPage,
                            sortField: sortField,
                            sortDirection: sortDirection,
                            _format: 'json',
                        };
            return Restangular.all('area/list').post(criteria, params);
        },
        get: function (id) {
            return Restangular.one('area',id).get();
        },
        getRoot: function (id) {
            return Restangular.one('area/root').get();
        },
        getChilds: function (id) {
            return Restangular.one('area', id).one('areas').get();
        },
        getAll: function () {
            return Restangular.one('area/all').get();
        },
        save: function (data) {
            return Restangular.one('area').post('?_format=json', data);
        },
        update: function (data) {
            return Restangular.one('area').customPUT(data);
        },
        delete: function (data) {
            return Restangular.one('area').remove();
        },
    };
}])

.service('PositionService', ['Restangular', function (Restangular) {
    return {
        search: function (currentPage, sortField, sortDirection, criteria) {
            var params = {
                            currentPage: currentPage,
                            sortField: sortField,
                            sortDirection: sortDirection,
                            _format: 'json',
                        };
            return Restangular.all('position/list').post(criteria, params);
        },
        get: function (id) {
                return Restangular.one('position', id).get();
        },
        getAll: function () {
            return Restangular.one('position/all').get();
        },
        getByArea: function (id) {
            return Restangular.one('position/area', id).one('positions').get();
        },
        save: function (data) {
            return Restangular.one('position').post('?_format=json', data);
        },
        update: function (data) {
                return Restangular.all('position/edit').customPUT(data);
        },
        remove: function (id) {
              return Restangular.one('position', id).remove();
        }
    }
}])

.service('EmployeeService', ['Restangular', function (Restangular) {
    return {
        search: function (currentPage, sortField, sortDirection, criteria) {
            var params = {
                currentPage: currentPage,
                sortField: sortField,
                sortDirection: sortDirection,
                _format: 'json',
            };
            return Restangular.all('employee/list').post(criteria, params);
        },
        get: function (id) {
            return Restangular.one('employee',id).get();
        },
        getAll: function (id) {
            return Restangular.one('employee/all',id).get();
        },
        getByPosition: function (id) {
            return Restangular.one('employee/position',id).one('employees').get();
        },
        save: function (data) {
            return Restangular.one('employee').post('save?_format=json', data);
        },
        update: function (data) {
            return Restangular.one('employee').customPUT(data);
        },
        delete: function (data) {
            return Restangular.one('employee').remove();
        },
    };
}])