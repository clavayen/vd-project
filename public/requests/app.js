angular.module('app', [ 'ngRoute',
                        'ui.router', 
                        'restangular',
                        'vd.controllers',
                        'vd.services',
                        'vd.area',
                        'vd.position',
                        'vd.employee',
                        'blockUI',
                    ])
                    
.constant("baseUrl", URL_BASE_SERVER)

.config(['$stateProvider','$httpProvider','$locationProvider','$urlRouterProvider','blockUIConfig',
	function ($stateProvider,$httpProvider,$locationProvider,$urlRouterProvider,blockUIConfig) {
	    $urlRouterProvider.otherwise('/areas/list');
	    blockUIConfig.message = 'Espere por favor..';
}])

.constant("NAVIGATION_ACTION",        
                        {"ACTION_LIST": 1,
                        "ACTION_NEW":   2,
                        "ACTION_EDIT":  3,
                        "ACTION_DELETE":4,
                        "ACTION_VIEW":5}
                            )


