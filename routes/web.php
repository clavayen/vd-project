<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function () use ($router) {
    return str_random(32);
});

$router->group(['prefix' => 'api/user'], function($router) {
    $router->get('/', 'UserController@show');
    $router->get('/{id}', 'UserController@getUser');
    $router->post('/', 'UserController@createUser');
    $router->put('/{id}', 'UserController@updateUser');
    $router->delete('/{id}', 'UserController@deleteUser');
});

$router->group(['prefix' => 'api/area'], function($router) {
    $router->get('/', 'AreaController@show');
    $router->post('/', 'AreaController@createArea');
    $router->get('/all', 'AreaController@getAll');
    $router->get('/root', 'AreaController@getAreasRoot');
    $router->get('/{id}', 'AreaController@getArea');
    $router->get('/{id}/areas', 'AreaController@getAreasByParent');
    $router->put('/{id}', 'AreaController@updateArea');
    $router->delete('/{id}', 'AreaController@deleteArea');
});

$router->group(['prefix' => 'api/position'], function($router) {
    $router->get('/', 'PositionController@show');
    $router->post('/', 'PositionController@createPosition');
    $router->get('/all', 'PositionController@getAll');
    $router->get('/{id}', 'PositionController@getPositionById');
    $router->get('/area/{id}/positions', 'PositionController@getPositionsByArea');
});

$router->group(['prefix' => 'api/employee'], function($router) {
    $router->get('/', 'EmployeeController@show');
    $router->post('/', 'EmployeeController@createEmployee');
    $router->get('/{id}', 'EmployeeController@getEmployee');
    $router->get('/position/{id}/employees', 'EmployeeController@getEmployeeByPosition');
});