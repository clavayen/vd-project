<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // root node
        factory(App\Models\Area::class,1)->create()->each(function ($area) {
            $position = factory(App\Models\Position::class)->make();
            $area->puestos()->save($position);

            $employees = factory(App\Models\Employee::class, 5)->make();
            $position->employees()->saveMany($employees);
        });
    }
}
