<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Area::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Root Vanesa Duran',
        'parent_id' => null,
        'created_at' =>$faker->date($format = 'Y-m-d', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});

$factory->define(App\Models\Position::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'created_at' =>$faker->date($format = 'Y-m-d', $max = 'now'),
        //'area_id' => factory('App\Models\Area')->create()->id,
        'updated_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});

$factory->define(App\Models\Employee::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'created_at' =>$faker->date($format = 'Y-m-d', $max = 'now'),
        'updated_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //'position_id' => factory('App\Models\Position')->create()->id,
    ];
});