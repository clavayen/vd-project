<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVdRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vd_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string("description");
            $table->string("status");

            $table->unsignedInteger('auth_process_id');
            $table->foreign('auth_process_id')
                ->references('id')
                ->on('vd_authorization_processes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vd_requests');
    }
}
