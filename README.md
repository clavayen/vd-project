# Vanesa Duran
Proyecto para la solicitudes de autorizacion

### Description
El siguiente proyecto fue desarrolado en un entorno dockerizado. El docker cuenta con servicios de Mysql, Nginx y Php.

### Prerequisites
Se debera contar con la siguiente herramientas instaladas:
1. Cliente de Git
2. Docker y Docker Compose
3. Node / Bower
4. Composer
5. Preferentemente Sistema Operativo Linux

### Installing
1. Ubicarse en la carpeta del proyecto clonado y ejecutar el comando:
composer install
2. Dar permisos de escritura a la carpeta Storage y Bootstrap
3. Ir a la carpeta public/request y ejecutar el comando: bower install
4. Ubicarse nuevamente en la raiz del proyecto y ejecutar el comando:
    sudo docker-compose up
5. Ejecutar el comando php artisan migrate
6. Ejecutar el comando php artisan db:seed

## Features
1. Arbol de Areas y Puestos
2. Creacion de Area
3. Creacion de Puesto
4. Ver Empleados creados por puesto.

## Deployment
Acceder a la url http://localhost:8080/requests/index.html#!/areas/list

## Authors
* **Jorge Villca**
* **Bruno Lavayen**

## License
Nada en la vida es gratis
