<?php
/**
 * Created by PhpStorm.
 * User: jvillca
 * Date: 26/10/18
 * Time: 17:11
 */

namespace App\Exceptions;


use Throwable;

class DuplicateException extends \Exception {
    protected $message;

    public function __construct(string $message = "")
    {
        parent::__construct($message);
    }

    public function __toString()
    {
        parent::__toString($this->message);
    }


}