<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 29/10/18
 * Time: 19:18
 */

namespace App\Interfaces;

use App\Models\Employee;


interface EmployeeService
{
    public function getEmployeeByPosition($id);
    public function getAll();

}