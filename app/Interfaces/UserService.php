<?php

namespace App\Interfaces;

interface UserService{

    public function createUser($params);

    public function searchUsersByFilter($params);
}
