<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 27/10/18
 * Time: 02:18
 */

namespace App\Interfaces;


use App\Models\Position;

interface PositionService {
    public function getPositionsByArea($id);
    public function createPosition(Position $position);
    public function getAll();
    public function getPositionById();
}