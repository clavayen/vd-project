<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 22/10/18
 * Time: 19:28
 */

namespace App\Interfaces;


use App\Models\Area;

interface AreaService{

    public function getAreasByParent($id);

    public function createArea(Area $area);
    public function getAreasRoot();
    public function getAll();
}