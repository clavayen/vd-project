<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 27/10/18
 * Time: 02:19
 */

namespace App\Repositories;


use App\Exceptions\DuplicateException;
use App\Interfaces\PositionService;
use App\Models\Position;

class PositionRepository implements PositionService {

    public function createPosition(Position $position){
        //validate unique name.
        $existAreaName = $this->existPositionName($position->name);

        if ($existAreaName){
            throw new DuplicateException("position already exist");
        }

        $position->save();
        return $position;
    }

    private function existPositionName($name, $positionId = null){
        $query = \App\Models\Area::query();
        $query->where('name', '=', $name);

        if ($positionId != null){
            $query->orWhereNotIn('id', $positionId);
        }

        $result = $query->get();

        if (count($result) > 0){
            return true;
        }
        return false;
    }

    public function getPositionsByArea($id = null){
        $query = Position::query();
        $query->where('area_id', '=', $id);
        return $query->get();
    }

    public function getAll(){
        $query = \App\Models\Position::query();
        return $query->with(['area'])->get();
    }

    public function getPositionById($id = null){
        $query = Position::query();
        $query->where('id', '=', $id);
        return $query->get();
    }

}