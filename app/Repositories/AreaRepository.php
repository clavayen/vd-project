<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 22/10/18
 * Time: 19:30
 */

namespace App\Repositories;


use App\Exceptions\DuplicateException;
use App\Interfaces\AreaService;
use App\Models\Area;

class AreaRepository implements AreaService{

    public function createArea(Area $area){
        //validate unique name.
        $existAreaName = $this->existAreaName($area->name);

        if ($existAreaName){
            throw new DuplicateException("area already exist");
        }

        $area->save();
        return $area;
    }

    private function existAreaName($name, $areaId = null){
        $query = \App\Models\Area::query();
        $query->where('name', '=', $name);

        if ($areaId != null){
            $query->orWhereNotIn('id', $areaId);
        }

        $result = $query->get();

        if (count($result) > 0){
            return true;
        }
        return false;
    }

    public function getAreasByParent($id = null){
        $query = \App\Models\Area::query();
        $query->where('parent_id', '=', $id);
        return $query->get();
    }

    public function getAreasRoot(){
        $query = \App\Models\Area::query();
        $query->where('parent_id', '=', null);
        return $query->get();
    }

    public function getAll(){
        $query = \App\Models\Area::query();
        return $query->get();
    }

}