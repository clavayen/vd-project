<?php

namespace App\Repositories;

use App\Interfaces\UserService;
use App\Models\Area;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserService{

    public function createUser($params){
        $result = app('db')->table('users')->insert([
            'email' => $params-email,
            'password' => app('hash')->make($params->password),
        ]);
        return $result;
    }

    public function searchUsersByFilter($params = null){
        if (is_null($params)){
            return Area::all();


        }

        $coco = new Area();
        $coco->name = str_random(5);
        $coco->save();

        $query = Area::query();

        if (isset($params['id']) && !empty($params['id'])){
            $query->where('id', '=', $params['id']);
        }
        if (isset($params['name']) && !empty($params['name'])){
            $conditionLike = "%" . $params['name'] ."%";
            $query->where('name', 'like', $conditionLike);
        }

        return $query->get();
    }
}
