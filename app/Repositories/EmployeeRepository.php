<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 29/10/18
 * Time: 19:23
 */

namespace App\Repositories;


use App\Interfaces\EmployeeService;
use App\Models\Employee;

class EmployeeRepository implements EmployeeService {

    public function getAll(){
        $query = Employee::query();
        return $query->get();
    }

    public function getEmployeeByPosition($id = null){
        $query = Employee::query();
        $query->with(['position'])->where('position_id', '=', $id);
        return $query->get();
    }
}