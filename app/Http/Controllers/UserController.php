<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserService;
use App\Models\User;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService){
        $this->userService = $userService;
    }

    public function show(){
        $params = array(
            'name' => 'test'
        );

        $users  = $this->userService->searchUsersByFilter($params);
        //$users  = User::all();
        return response()->json($users);
    }

}
