<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 29/10/18
 * Time: 19:17
 */

namespace App\Http\Controllers;

use App\Interfaces\EmployeeService;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class EmployeeController extends Controller{

    private $service;

    public function __construct(EmployeeService $service){
        $this->service = $service;
    }

    public function getEmployeeByPosition($id){
        $employees  = $this->service->getEmployeeByPosition($id);
        return response()->json(['data' => $employees], 200);
    }

}