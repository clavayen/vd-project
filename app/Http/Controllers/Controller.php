<?php

namespace App\Http\Controllers;

use App\Utils\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController {

    public $response;

    /**
     * Controller constructor.
     * @param $data
     * @param $code
     * @param $message
     * @param $status
     */
    public function __construct($data, $code, $message, $status)
    {
        $this->response = new Response();
    }
}
