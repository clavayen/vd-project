<?php

namespace App\Http\Controllers;

use App\Interfaces\PositionService;
use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionController extends Controller {
    private $service;

    public function __construct(PositionService $service){
        $this->service = $service;
    }

    public function createPosition(Request $request){
        try {
            $newPosition = Position::getPosition($request);
            $this->service->createPosition($newPosition);
            return response()->json(['data' => $newPosition], 200);
        } catch (DuplicateException $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        } catch (Exception $ex) {
            return response()->json(['error' => 'There was an error saving the area.'], 500);
        }
    }

    public function getPositionsByArea($id){
        $positions  = $this->service->getPositionsByArea($id);
        return response()->json(['data' => $positions], 200);
    }

    public function getAll(){
        $positions = $this->service->getAll();
        return response()->json(['data' => $positions], 200);
    }

    public function getPositionById($id){
        $positions  = $this->service->getPositionById($id);
        return response()->json(['data' => $positions], 200);
    }


}
