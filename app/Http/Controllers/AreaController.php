<?php

namespace App\Http\Controllers;

use App\Exceptions\DuplicateException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\AreaService;
use App\Models\Area;

class AreaController extends Controller
{
    private $service;

    public function __construct(AreaService $service){
        $this->service = $service;
    }

    public function getAll(){
        $areas = $this->service->getAll();
        return response()->json(['data' => $areas], 200);
    }

    public function createArea(Request $request){
        try {
            $newArea = Area::getArea($request);
            $this->service->createArea($newArea);
            return response()->json(['data' => $newArea], 200);
        } catch (DuplicateException $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        } catch (Exception $ex) {
            return response()->json(['error' => 'There was an error saving the area.'], 500);
        }
    }

    public function getAreasByParent($id){
        $areas  = $this->service->getAreasByParent($id);
        return response()->json(['data' => $areas], 200);
    }

    public function getAreasRoot(){
        $areas = $this->service->getAreasRoot();
        return response()->json(['data' => $areas], 200);
    }

}
