<?php
/**
 * Created by PhpStorm.
 * User: coco
 * Date: 27/10/18
 * Time: 02:02
 */

class Response {
    public $data;
    public $code;
    public $message;
    public $status;

    function __construct() {

    }

    public function getData() {
        return $this->data;
    }

    public function getCode() {
        return $this->code;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

}