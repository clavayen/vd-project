<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Interfaces\UserService','App\Repositories\UserRepository');
        $this->app->bind('App\Interfaces\AreaService','App\Repositories\AreaRepository');
        $this->app->bind('App\Interfaces\PositionService','App\Repositories\PositionRepository');
        $this->app->bind('App\Interfaces\EmployeeService','App\Repositories\EmployeeRepository');
    }
}
