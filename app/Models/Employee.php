<?php
/**
 * Created by PhpStorm.
 * User: jvillca
 * Date: 26/10/18
 * Time: 14:37
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{
    protected $table = 'vd_employees';

    protected $fillable = [];
    protected $hidden = [];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }
}