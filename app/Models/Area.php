<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'vd_areas';

    protected $fillable = ["name","parent_id", "created_at","updated_at"];
    protected $hidden = [""];

    public function puestos(){
        return $this->hasMany(Position::class);
    }

    public function parent()
    {
        return $this->belongsTo(Area::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Area::class, 'parent_id');
    }

    public static function getArea(\Illuminate\Http\Request $request){
        $new = new Area();
        $new->name = $request->input('name');
        $new->parent_id = $request->input('parent_id');
        $new->created_at = Carbon::now()->toDateTimeString();
        return $new;
    }
}
