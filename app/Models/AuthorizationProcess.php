<?php
/**
 * Created by PhpStorm.
 * User: jvillca
 * Date: 26/10/18
 * Time: 15:25
 */

namespace App\Models;


class AuthorizationProcess
{
    protected $table = 'vd_authorization_processes';

    protected $fillable = ["name"];
    protected $hidden = [];


}