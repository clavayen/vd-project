<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'vd_positions';

    protected $fillable = [];
    protected $hidden = [];
    

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function employees(){
        return $this->hasMany(Employee::class);
    }

    public static function getPosition(\Illuminate\Http\Request $request){
        $new = new Position();
        $new->name = $request->input('name');
        $new->area_id = $request->input('area_id');
        $new->created_at = Carbon::now()->toDateTimeString();
        return $new;
    }
}
