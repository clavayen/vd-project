<?php
/**
 * Created by PhpStorm.
 * User: jvillca
 * Date: 26/10/18
 * Time: 14:38
 */

namespace App\Models;


class Request extends Model
{
    protected $table = 'vd_requests';

    protected $fillable = ["employee_id"];
    protected $hidden = [];


}